---
layout: post
title: 专注前端开发
subtitle: Jekyll is a blog-aware, static site generator in Ruby https://jekyllrb.com
date: 2015-03-02 11:00:32
author: clearives
---

![专注前端开发](http://7othoq.com1.z0.glb.clouddn.com/web.jpg "专注前端开发")

欢迎大家来到[我的博客](http://clearives.github.io/)，最近手上没项目，看看自己原来的[博客](http://www.2ives.com/)也好久没更新了，刚好最近喜欢上了markdown，于是我就打算在Github上写博客了，用的是Hexo，部署过程很简单，很喜欢这种方式来写博客。


<!--more-->
原来的[博客](http://www.2ives.com/)一部分文章已经迁移过来,以后就是这个了。
#### 关于博主

ID:Clearives
学历：本科
爱好：摄影
职业：Html5,前端开发
掌握技术：Html5,Css3,Javascript,React,Webpack,PS...
个人站点：http://clearives.cc
新浪微博：http://weibo.com/clearives
目前居住：广东深圳

我还是个菜鸟，但我觉得我们可以交流，本人热衷与前端开发技术，对这个领域的新技术很着迷，目前在研究MV*,如果你也一样热爱前端，我觉得我们可以成为朋友，当然不一样，也行哈~我这人热情，哈哈

#### 个人项目

<!--* [环球海淘商城](http://test.haitaotmall.com/)  (很遗憾这个项目快要上线了，就GG了~)-->
* Small Cell微信DM
* 华为LTE eMBMS宣传项目
* VGS 手机版，VGS Ipad版
* SmartPCC自定义use case
* 还有一些微信项目(定制化案例)
* [Liveapp](http://liveapp.cn/)
* [广告效果监测平台](http://t2.dataeye.com/pages/index.jsp?token=41a2e70b7a827b9bc2c4123b3adf3a752d40083fd8cc926bf0e26580b8ea471d)
* [Splus官网](http://splus.cn/)
我知道这些可能不值得一提，但是不要紧，要看项目情况的可以找我。

```
console.log(1)
```


#### 联系方式

Email:　<a href="mailto:clearives@gmail.com">clearives@gmail.com</a>
Weibo: [@曾祥辉_](http://weibo.com/clearives)  (欢迎关注~)
QQ: 704219713
